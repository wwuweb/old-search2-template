# Search Template #

* index.html is just one sample output of search for purposes of developing the styles locally. It has the header and footer html, and the custom style sheet manually inserted. It has no use in any real application.
* header.html has the header elements, and the opening wrapper tag for the search area
* footer.html has the closing wrapper tag for the search area and the footer elements
* custom-search-styles.css has all the styling for the search page